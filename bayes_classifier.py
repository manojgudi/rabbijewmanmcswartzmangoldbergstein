from __future__ import division


file_obj = open('SensorLog_22346-10202013.csv', 'r')
data = file_obj.read()
file_obj.close()

#print data

data_split = data.replace('\n',';').split(";")

def extract_terms(terms_list, multiple):
	selective_terms = []
	for i in range(multiple, len(terms_list), 18):
		try:
			float_val = float(terms_list[i])
			selective_terms.append(float_val)
		except:
			pass
	return selective_terms

# To pop out the first text of category append *.pop(0)

time_stamp = extract_terms(data_split, 0)
accx = extract_terms(data_split, 3)
accy = extract_terms(data_split, 4)
accz = extract_terms(data_split, 5)
gyrox = extract_terms(data_split, 6)
gyroy = extract_terms(data_split, 7)
gyroz = extract_terms(data_split, 8)
yaw = extract_terms(data_split, 12)
pitch = extract_terms(data_split, 13)
roll_val = extract_terms(data_split, 14)

### PLOTTING ###
import matplotlib.pyplot as plt
from pylab import *

figure(0)
plt.plot(accx)

'''
figure(1)
plt.plot(accy)

figure(2)
plt.plot(accz)

figure(3)
plt.plot(gyrox)


figure(4)
plt.plot(gyroy)


figure(5)
plt.plot(gyroz)


figure(6)
plt.plot(yaw)


figure(7)
plt.plot(pitch)


figure(8)
plt.plot(roll_val)
'''
#plt.show()
import numpy as np


def get_err(data_array):
	mean = sum(data_array)/len(data_array)

	err_vec = [ x - mean for x in data_array ]
	return err_vec

def two_class_classify(data_array, threshold_of_mean):
	err_vec = get_err(data_array)
	good_values = []
	err_values = []
	
	for value, data in zip(err_vec, data_array):
		if value >= threshold_of_mean :
			good_values.append(data)
		else:
			err_values.append(data)
	
	p_good_values = len(good_values)/(len(good_values)+len(err_values))

	return good_values, err_values, p_good_values, 1-p_good_values

# 0.1 is Threshold, you can program it as 0.1*mean, 90% interval
accx_good_values, accx_err_values, p_good, p_err  = two_class_classify(accx, 0.1*np.mean(accx))


# Discriminant 
# g(x) = P(w1 | x) - P(w2 | x)
# g(x) = w X - w0
# where w = 1 by sigma^2 times mu
# and w0 = log(P(omega) - 1/2 mu square times inverse sigma square
# since these are univariate

def get_coeff(omega, p_omega):
	mu = np.mean(omega)
	sig = np.std(omega, ddof = 1) # It is importand to set ddof=1 to have unbiased variance (dividing by n-1)
	w = sig**-2 * mu
	
	import math
	w0 = math.log(p_omega) - (0.5 * (mu**2) * (sig**-2))
	return [w,w0]

def get_g_val ( data_array, omega, p_omega):
	[w, w0] = get_coeff(omega, p_omega)
	g_val = [ (w*x + w0) for x in data_array]

	return g_val

print get_coeff(accx_good_values, p_good)
print get_coeff(accx_err_values, p_err)

# g1(x) - g2(x) 
diffs= [ float(x1 - x2)  for x1, x2 in zip(get_g_val(accx, accx_good_values, p_good), get_g_val(accx, accx_err_values, p_err))]
 
for diff in diffs:
	if diff > 0:
		print "good: " +str(diff)
	else:	
		print "err: "+str(diff)
